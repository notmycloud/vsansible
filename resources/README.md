# Ansible Favicons

These icons are useful to differentiate between this Ansible Code-Server and other Code-Servers you may have running.

The source icon is provided by [Simple Icons](https://simpleicons.org/?q=ansible)

## ImageMagick

Use these commands to generate the PNG and ICO files from the source SVG.

```
convert -density 256x256 -background transparent ansible.svg -define icon:auto-resize -colors 256 pwa-icon.png
convert -density 192x192 -background transparent ansible.svg -define icon:auto-resize -colors 256 pwa-icon-192.png
convert -density 512x512 -background transparent ansible.svg -define icon:auto-resize -colors 256 pwa-icon-512.png
convert -density 256x256 -background transparent ansible.svg -define icon:auto-resize -colors 256 favicon.ico
cp ansible.svg favicon.svg
cp favicon.svg favicon-dark-support.svg
```