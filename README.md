# VSAnsible - Work in progress.

This image combines VS Code-Server and Ansible so you can easily spin up an Ansible node and edit and run playbooks in your web browser.

Feel free to open issues at the parent repo: https://gitlab.com/notmycloud/vsansible/

Docker Image is based on the codercom/code-server, please reference their documentation for initial setup.
https://github.com/coder/code-server

## Setup

### Entrypoint

~~Mount your workspace setup scripts to `$HOME/entrypoint.d/*.sh`.  
These will be executed as part of the container entrypoint as the `coder` user.  
`*.sh` files will have `chmod u+x` ran on them before looping through.  
This may be useful for install PIP dependancies or code-server extensions.~~

Starting with Coder Server v4.5.0 any executables in the directory specified by the `ENTRYPOINTD` environment variable will be run on container start.

### Ansible

Mount your Ansible workspace to `/ansible`.  
Mitogen is pre-installed and the path is saved to the `MITOGEN` environmental variable.  
It can also be found by running the following command in the container.  
`find / -type d 2>/dev/null | grep "ansible_mitogen/plugins" | sort | head -n 1`  
This should return: `/home/coder/.local/lib/python3.9/site-packages/ansible_mitogen/plugins`

### VSCode

VSCode configuration is saved to `/home/coder/.local/share/code-server`.  
VSCode extensions can be installed by creating an entrypoint script as defined above with the following command.
`code-server --install-extension <extension id>`  
For example: `code-server --install-extension shan.code-settings-sync`  
I recomend using this extension to sync your settings, at least until code-server supports [native sync](https://github.com/coder/code-server/issues/2195). This extension will save your settings and extensions to a Github Gist.  
You must access VSAnsible from an HTTPS session for setup to work. When you get the callback of `localhost:54321/callback?code=SecretCode`, curl it into a terminal window in VSCode.  
`curl localhost:54321/callback?code=SecretCode`  

### Code Server

Code-server configuration is saved to `/home/coder/.config/code-server/config.yaml`.  
You can pass command line arguments to your Docker Run command to be passed on to the code-server instance.
```
code-server 4.3.0 09bc30027a7fbba170f907a527eaa9f7219fe739 with Code 1.65.2

Usage: code-server [options] [path]
    - Opening a directory: code-server ./path/to/your/project
    - Opening a saved workspace: code-server ./path/to/your/project.code-workspace

Options
      --auth                   The type of authentication to use. [password, none]
      --password               The password for password authentication (can only be passed in via $PASSWORD or the config file).
      --hashed-password        The password hashed with argon2 for password authentication (can only be passed in via $HASHED_PASSWORD or the config file).
                               Takes precedence over 'password'.
      --cert                   Path to certificate. A self signed certificate is generated if none is provided.
      --cert-host              Hostname to use when generating a self signed certificate.
      --cert-key               Path to certificate key when using non-generated cert.
      --disable-telemetry      Disable telemetry.
      --disable-update-check   Disable update check. Without this flag, code-server checks every 6 hours against the latest github release and
                               then notifies you once every week that a new release is available.
      --disable-file-downloads Disable file downloads from Code.
   -h --help                   Show this output.
      --open                   Open in browser on startup. Does not work remotely.
      --bind-addr              Address to bind to in host:port. You can also use $PORT to override the port.
      --config                 Path to yaml config file. Every flag maps directly to a key in the config file.
      --socket                 Path to a socket (bind-addr will be ignored).
      --socket-mode            File mode of the socket.
   -v --version                Display version information.
      --user-data-dir          Path to the user data directory.
      --extensions-dir         Path to the extensions directory.
      --list-extensions        List installed VS Code extensions.
      --force                  Avoid prompts when installing VS Code extensions.
      --install-extension      Install or update a VS Code extension by id or vsix. The identifier of an extension is `${publisher}.${name}`.
                               To install a specific version provide `@${version}`. For example: 'vscode.csharp@1.2.3'.
      --enable-proposed-api    Enable proposed API features for extensions. Can receive one or more extension IDs to enable individually.
      --uninstall-extension    Uninstall a VS Code extension by id.
      --show-versions          Show VS Code extension versions.
      --github-auth            GitHub authentication token (can only be passed in via $GITHUB_TOKEN or the config file).
      --proxy-domain           Domain used for proxying ports.
   -e --ignore-last-opened     Ignore the last opened directory or workspace in favor of an empty window.
   -n --new-window             Force to open a new window.
   -r --reuse-window           Force to open a file or folder in an already opened window.
 -vvv --verbose                Enable verbose logging.
      --link                   (deprecated) Securely bind code-server via our cloud service with the passed name. You'll get a URL like
                               https://hostname-username.coder.co at which you can easily access your code-server instance.
                               Authorization is done via GitHub.
```

### Docker/Podman

Basic usage: `docker run --rm -it -v $(pwd):/ansible notmycloud/vsansible:${CODE_SERVER_VERSION}-${ANSIBLE_VERSION}`  
For example, the following command will run a v4 code-server with v5 of Ansible (community edition).  
`docker run --rm -it -v $(pwd):/ansible notmycloud/vsansible:v4-v5`  

### Configuration

Default configuration in the home folder, can be overwritten or overridden by placing an ansible.cfg in /ansible.
```
inventory = /ansible/hosts.yaml
library = /ansible/library
gathering = smart
inject_facts_as_vars = False
roles_path  = /ansible/.galaxy_roles:/ansible/roles
callbacks_enabled = ansible.posix.profile_tasks, ansible.posix.profile_roles
log_path = /dev/stdout
strategy_plugins = ~/.local/lib/python3.9/site-packages/ansible_mitogen/plugins
strategy = mitogen_linear
pipelining = True
```
