# docker build -t container_tag --build-arg CODE_SERVER_IMAGE_ARG=codercom/code-server --build-arg CODE_SERVER_VERSION_ARG=latest --build-arg ANSIBLE_VERSION_ARG=5.6.0 .

# https://github.com/coder/code-server
# https://github.com/coder/code-server/blob/main/ci/release-image/Dockerfile
ARG CODE_SERVER_IMAGE_ARG "codercom/code-server"
ARG CODE_SERVER_VERSION_ARG "latest"
FROM ${CODE_SERVER_IMAGE_ARG}:${CODE_SERVER_VERSION_ARG}
ENV CODE_SERVER_VERSION ${CODE_SERVER_VERSION_ARG}

LABEL maintainer="docker@notmy.cloud"
LABEL org.label-schema.schema-version="2.0.1"
LABEL org.label-schema.name="notmycloud/vsansible"
LABEL org.label-schema.description="Ansible with VS Code-Server inside an OCI Container"
LABEL org.label-schema.url="https://gitlab.com/notmycloud/vsansible"
LABEL org.label-schema.vcs-url="https://gitlab.com/notmycloud/vsansible"
LABEL org.label-schema.vendor="Not My Cloud"


# Include Ansible in Code Server, based off of willhallonline's Dockerfile.
# https://github.com/willhallonline/docker-ansible/blob/master/ansible/debian-bullseye/Dockerfile

# Reset to root user instead of coder from base image
USER root
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV DEBIAN_FRONTEND noninteractive

RUN echo "export PATH=\$HOME/.local/bin${PATH:+:${PATH}}" >> /etc/profile

RUN apt-get update && \
    apt-get --no-install-recommends install -y git gcc libhdf5-dev libssl-dev libffi-dev openssh-client python3-pip sshpass && \
    apt-get --no-install-recommends install -y gpg wget libcap2-bin && \
    wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg >/dev/null && \
    gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint && \
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list && \
    apt-get update && \
    apt-get --no-install-recommends install -y python3-dev libkrb5-dev krb5-user vault && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

# TODO: Windows Auth
# Install krb5-libs krb5-workstation not found in Bullseye
# Removed to slim down size
#    apt-get --no-install-recommends install -y bind9-utils dbus inotify-tools && \

RUN mkdir /ansible && \
    chown 1000:1000 /ansible && \
    mkdir -p /etc/ansible && \
    echo 'localhost' > /etc/ansible/hosts

COPY resources/* /usr/lib/code-server/src/browser/media/

# This way, if someone sets $DOCKER_USER, docker-exec will still work as
# the uid will remain the same. note: only relevant if -u isn't passed to
# docker-run.
USER 1000
    
RUN pip3 install --no-warn-script-location --upgrade pip cffi && \
    pip3 install --no-warn-script-location mitogen jmespath && \
    pip3 install --no-warn-script-location --upgrade  pywinrm>=0.3.0 pywinrm[kerberos] && \
    pip3 install --no-warn-script-location paramiko proxmoxer requests && \
    pip3 install --no-warn-script-location netaddr jinja2>=2.11.2 && \
    pip3 install --no-warn-script-location ssh-audit dnspython && \
    pip3 install --no-warn-script-location hvac github3.py && \
    rm -rf $HOME/.cache/pip

RUN echo "export MITOGEN=$(find / -type d 2>/dev/null | grep "ansible_mitogen/plugins" | sort | head -n 1)" >> $HOME/.bash_profile

# Separated out for caching purposes
# https://pypi.org/project/ansible/#history
ARG ANSIBLE_VERSION_ARG "6.3.0"
ENV ANSIBLE_VERSION ${ANSIBLE_VERSION_ARG}
RUN pip3 install --no-warn-script-location ansible==${ANSIBLE_VERSION} && \
    rm -rf $HOME/.cache/pip

RUN pip3 install --no-warn-script-location "ansible-lint" && \
    rm -rf $HOME/.cache/pip

# ADD https://raw.github.com/ansible/ansible/devel/examples/ansible.cfg /etc/ansible/ansible.cfg
# Since Ansible 2.12 (core):
# To generate an example config file (a "disabled" one with all default settings, commented out):
#               $ ansible-config init --disabled > ansible.cfg
#
# Also you can now have a more complete file by including existing plugins:
# ansible-config init --disabled -t all > ansible.cfg

# For previous versions of Ansible you can check for examples in the 'stable' branches of each version
# Note that this file was always incomplete  and lagging changes to configuration settings
# Disabled as it gives an access denied error, will look into later.
# RUN sudo ansible-config init --disabled -t all > /etc/ansible/ansible.cfg

COPY --chown=coder:coder ansible.cfg $HOME/.ansible.cfg
WORKDIR /ansible

LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.docker.cmd="docker run --rm -it -v $(pwd):/ansible notmycloud/vsansible:${CODE_SERVER_VERSION}-${ANSIBLE_VERSION}"
